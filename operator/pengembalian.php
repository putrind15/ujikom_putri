<!DOCTYPE html>
<?php
include 'cek.php';
?>
<html lang="en">
<head>
	
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>Inventaris Sarana dan Prasarana SMK</title>
	<meta name="description" content="Bootstrap Metro Dashboard">
	<meta name="author" content="Dennis Ji">
	<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<!-- end: Meta -->
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	
	<!-- start: CSS -->
	<link id="bootstrap-style" href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->
	

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->
		
	<!-- start: Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico">
	<!-- end: Favicon -->
	
		
		
		
</head>

<body>
		<!-- start: Header -->
	<div class="navbar">
		<div class="blue">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href=""><span>Inventaris</span></a>
					
				<!-- start: Header Menu -->
				<div class="nav-no-collapse header-nav">
					<ul class="nav pull-right">
						
						
						<!-- start: User Dropdown -->
						<li class="dropdown">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="halflings-icon white user"></i> <?php echo $_SESSION['nama_petugas'];?>
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li><a href="profil.php"><i class="halflings-icon user"></i> Profil</a></li>
								<li><a href="logout.php"><i class="halflings-icon off"></i> Logout</a></li>
							</ul>
						</li>
						<!-- end: User Dropdown -->
					</ul>
				</div>
				<!-- end: Header Menu -->
				
			</div>
		</div>
	</div>
	<!-- start: Header -->
	
		<div class="container-fluid-full">
		<div class="row-fluid">
				
			<!-- start: Main Menu -->
			<div id="sidebar-left" class="span2">
				<div class="nav-collapse sidebar-nav">
					<ul class="nav nav-tabs nav-stacked main-menu">
						<li><a href="dashboard.php"><i class="icon-home"></i><span class="hidden-tablet"> Dashboard</span></a></li>	
						<li><a href="peminjaman.php"><i class="icon-tasks"></i><span class="hidden-tablet"> Peminjaman</span></a></li>	
						<li><a href="pengembalian.php"><i class="icon-tasks"></i><span class="hidden-tablet"> Pengembalian</span></a></li>	
						</ul>
				</div>
			</div>
			<!-- end: Main Menu -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<!-- start: Content -->
			<div id="content" class="span10">
			<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon white user"></i><span class="break"></span>Pengembalian</h2>
						</div>
					<div class="box-content">
					<center><div class="panel-body">
                        <div class="col-lg-3 col-md-offset-4">
                <label>Pilih Id Peminjaman</label>
                <form method="POST">
                    <select name="id_peminjaman" class="form-control m-bot15">
                        <?php
                        include "koneksi.php";
                                //display values in combobox/dropdown
                        $result = mysql_query("SELECT id_peminjaman from peminjaman where status_peminjaman='Pinjam' ");
                        while($row = mysql_fetch_assoc($result))
                        {
                            echo "<option value='$row[id_peminjaman]'>$row[id_peminjaman]</option>";
                        } 
                        ?>
                   </select>
                                    <br/>
                                <button type="submit" name="pilih" class="btn btn-outline btn-primary">Tampilkan</button>
                            </form>
                        </div>
                    </div>
					    <?php
                if(isset($_POST['pilih'])){?>
                  <form action="proses_pengembalian.php" method="post" enctype="multipart/form-data">
                     <?php
                     include "koneksi.php";
                     $id_peminjaman=$_POST['id_peminjaman'];
                     $select=mysql_query("select * from peminjaman s left join detail_pinjam d on d.id_detail_pinjam=s.id_peminjaman
												left join inventaris i on d.id_inventaris=i.id_inventaris
												left join pegawai p on p.id_pegawai=s.id_pegawai
                        where id_peminjaman='$id_peminjaman'");
                     while($data=mysql_fetch_array($select)){
                        ?>
                <div class="form-group">
                    <label>Inventaris</label>
                    <input name="id_peminjaman" type="hidden" class="form-control"  placeholder="Masukan Jumlah barang" value="<?php echo $data['id_peminjaman'];?>" autocomplete="off" maxlength="11" required="">
                    <input name="id_detail_pinjam" type="hidden" class="form-control" placeholder="Masukan Jumlah barang" value="<?php echo $data['id_detail_pinjam'];?>" autocomplete="off" maxlength="11" required="">
                    <input name="id_inventaris" type="text" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $data['nama'];?>" autocomplete="off" maxlength="11" required="">
                </div><br>                    
               
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >
                        Tanggal Pinjam
                    </label>
                    <div class="col-md-6 col-sm-6o col-xs-12">
                        <input type="datetime" name="" value="<?php echo $data['tanggal_pinjam']?>" class="form-control col-md-7 col-xs-12"  placeholder="tanggal_pinjam">
                        <input type="hidden" name="tanggal_pinjam" value="<?php echo $data['tanggal_pinjam']?>" >
                        <input type="hidden" name="tanggal_kembali" >
                    </div>
                </div><br><br><br>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >
                        Nama Pegawai
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="" value="<?php echo $data['nama_pegawai']?>" class="form-control col-md-7 col-xs-12"  placeholder="ID Pegawai">
                        <input type="hidden" name="id_pegawai" value="<?php echo $data['id_pegawai']?>" >
                    </div>
                </div><br><br><br>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >
                        Jumlah
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="jumlah_pinjam" value="<?php echo $data['jumlah_pinjam']?>" class="form-control col-md-7 col-xs-12"  placeholder="Jumlah">
                    </div>
                </div><br><br>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >
                     Status Peminjaman
                 </label>
                 <div class="col-md-6 col-sm-6 col-xs-12">
                  <select name="status_peminjaman" class="form-control m-bot15">
                    <option><?php echo $data['status_peminjaman']?></option>
                    <option>Kembali</option>

                </select>
            </div><br><br><br>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </div>

    <?php } ?>
</form>

<?php } ?>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <br><br>
						  <thead>
							  <tr>
								  <th>No</th>
								  <th>ID Peminjaman</th>
								  <th>Tanggal Pinjam</th>
								  <th>Tanggal Kembali</th>
								  <th>Pegawai</th>
								  <th>Status Peminjaman</th>
							  </tr>
						  </thead>
						  <tbody>
							<?php
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "ujikom";
 
            // Membuat Koneksi
            $koneksi = new mysqli($servername, $username, $password, $dbname);
            
            // Melakukan Cek Koneksi
            if ($koneksi->connect_error) {
                die("Koneksi Gagal : " . $koneksi->connect_error);
            } 
 
            //Melakukan query
            $sql = "SELECT * FROM peminjaman s left join pegawai p on p.id_pegawai=s.id_pegawai where status_peminjaman='Kembali'";
            $hasil = $koneksi->query($sql);
            $no = 1;
            if ($hasil->num_rows > 0) {
                foreach ($hasil as $row) { ?>
                  <tr class="success">     
                  <td><?php echo $no; ?></td>
                  <td><?php echo $row['id_peminjaman']; ?></td>
                  <td><?php echo $row['tanggal_pinjam']; ?></td>
                  <td><?php echo $row['tanggal_kembali']; ?></td>
				  <td><?php echo $row['nama_pegawai']; ?></td>
				  <td><?php echo $row['status_peminjaman']; ?></td>
				  </tr>
            <?php 
            $no++; 
            } 
              } else { 
            echo "0 results"; 
              } $koneksi->close(); 
            ?>
          	      
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
					</div></center>
			
			</div><!--/row-->

	</div><!--/.fluid-container-->
	
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
		
	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Settings</h3>
		</div>
		<div class="modal-body">
			<p>Here settings can be configured...</p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
			<a href="#" class="btn btn-primary">Save changes</a>
		</div>
	</div>
	<div class="common-modal modal fade" id="common-Modal1" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-content">
			<ul class="list-inline item-details">
				<li><a href="http://themifycloud.com">Admin templates</a></li>
				<li><a href="http://themescloud.org">Bootstrap themes</a></li>
			</ul>
		</div>
	</div>
	
	
  
	<div class="clearfix"></div>
	
	<footer>

		<p>
			<span style="text-align:left;float:left">&copy; 2019 Inventaris Sarana dan Prasarana SMK</a></span>
			
		</p>

	</footer>
	
	<!-- start: JavaScript-->

		<script src="js/jquery-1.9.1.min.js"></script>
	<script src="js/jquery-migrate-1.0.0.min.js"></script>
	
		<script src="js/jquery-ui-1.10.0.custom.min.js"></script>
	
		<script src="js/jquery.ui.touch-punch.js"></script>
	
		<script src="js/modernizr.js"></script>
	
		<script src="js/bootstrap.min.js"></script>
	
		<script src="js/jquery.cookie.js"></script>
	
		<script src='js/fullcalendar.min.js'></script>
	
		<script src='js/jquery.dataTables.min.js'></script>

		<script src="js/excanvas.js"></script>
	<script src="js/jquery.flot.js"></script>
	<script src="js/jquery.flot.pie.js"></script>
	<script src="js/jquery.flot.stack.js"></script>
	<script src="js/jquery.flot.resize.min.js"></script>
	
		<script src="js/jquery.chosen.min.js"></script>
	
		<script src="js/jquery.uniform.min.js"></script>
		
		<script src="js/jquery.cleditor.min.js"></script>
	
		<script src="js/jquery.noty.js"></script>
	
		<script src="js/jquery.elfinder.min.js"></script>
	
		<script src="js/jquery.raty.min.js"></script>
	
		<script src="js/jquery.iphone.toggle.js"></script>
	
		<script src="js/jquery.uploadify-3.1.min.js"></script>
	
		<script src="js/jquery.gritter.min.js"></script>
	
		<script src="js/jquery.imagesloaded.js"></script>
	
		<script src="js/jquery.masonry.min.js"></script>
	
		<script src="js/jquery.knob.modified.js"></script>
	
		<script src="js/jquery.sparkline.min.js"></script>
	
		<script src="js/counter.js"></script>
	
		<script src="js/retina.js"></script>

		<script src="js/custom.js"></script>
	<!-- end: JavaScript-->
	
</body>
</html>
