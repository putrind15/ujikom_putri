<?php ob_start(); ?>
<html>
<head>
  <title>Cetak PDF</title>
    
   <style>
   table {border-collapse:collapse; table-layout:fixed;width: 630px; }
   table td {word-wrap:break-word;width: 20%; }
   </style>
</head>
<body>
  
<h1 style="text-align: center; color:black;">Data Ruang</h1>
<table border="1" align="center" width="100%">
<tr>
  <th>Kode ruang</th>
  <th>Nama</th>
  <th>Keterangan</th>
  </tr>
<?php
// Load file koneksi.php
include "koneksi.php";
 
$query = "select * from ruang"; // Tampilkan semua data gambar
$sql = mysql_query( $query); // Eksekusi/Jalankan query dari variabel $query
$row = mysql_num_rows($sql); // Ambil jumlah data dari hasil eksekusi $sql
 
if($row > 0){ // Jika jumlah data lebih dari 0 (Berarti jika data ada)
    while($data = mysql_fetch_array($sql)){ // Ambil semua data dari hasil eksekusi $sql
        echo "<tr>";
        echo "<td>".$data['nama_ruang']."</td>";
		echo "<td>".$data['kode_ruang']."</td>";
		echo "<td>".$data['keterangan']."</td>";
        echo "</tr>";
    }
}else{ // Jika data tidak ada
    echo "<tr><td colspan='4'>Data tidak ada</td></tr>";
}
?>
</table>

</body>
</html>
<?php
$html = ob_get_contents();
ob_end_clean();
        
require_once('html2pdf/html2pdf.class.php');
$pdf = new HTML2PDF('p','A4','en');
$pdf->WriteHTML($html);
$pdf->Output('Data Ruang.pdf', 'D');
?>
