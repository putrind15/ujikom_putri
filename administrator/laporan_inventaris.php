<?php ob_start(); ?>
<html>
<head>
  <title>Cetak PDF</title>
    
   <style>
   table {border-collapse:collapse; table-layout:fixed;width: 630px; }
   table td {word-wrap:break-word;width: 11%; }
   </style>
</head>
<body>
  
<h1 style="text-align: center; color:black;">Data Barang</h1>
<table border="1" width="200%">
<tr>
  <th>Kode Inventaris</th>
  <th>Nama</th>
  <th>Kondisi</th>
  <th>Keterangan</th>
  <th>jumlah</th>
  <th>jenis</th>
  <th>Tanggal Register</th>
  <th>Ruang</th>
  <th>petugas</th>
</tr>
<?php
// Load file koneksi.php
include "koneksi.php";
 
$query = "select * from inventaris i 
					left join jenis j on i.id_jenis=j.id_jenis
					left join ruang r on i.id_ruang=r.id_ruang
					left join petugas p on i.id_jenis=p.id_petugas"; // Tampilkan semua data gambar
$sql = mysql_query( $query); // Eksekusi/Jalankan query dari variabel $query
$row = mysql_num_rows($sql); // Ambil jumlah data dari hasil eksekusi $sql
 
if($row > 0){ // Jika jumlah data lebih dari 0 (Berarti jika data ada)
    while($data = mysql_fetch_array($sql)){ // Ambil semua data dari hasil eksekusi $sql
        echo "<tr>";
        echo "<td>".$data['kode_inventaris']."</td>";
		echo "<td>".$data['nama']."</td>";
		echo "<td>".$data['kondisi']."</td>";
		echo "<td>".$data['keterangan']."</td>";
        echo "<td>".$data['jumlah']."</td>";
		echo "<td>".$data['nama_jenis']."</td>";
		echo "<td>".$data['tanggal_register']."</td>";
		echo "<td>".$data['nama_ruang']."</td>";
		echo "<td>".$data['nama_petugas']."</td>";
        echo "</tr>";
    }
}else{ // Jika data tidak ada
    echo "<tr><td colspan='4'>Data tidak ada</td></tr>";
}
?>
</table>

</body>
</html>
<?php
$html = ob_get_contents();
ob_end_clean();
        
require_once('html2pdf/html2pdf.class.php');
$pdf = new HTML2PDF('L','A4','en');
$pdf->WriteHTML($html);
$pdf->Output('Data Barang.pdf', 'D');
?>
