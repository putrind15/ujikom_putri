<!DOCTYPE html>
<?php
include 'cek.php';
?>
<html lang="en">
<head>
	
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>Inventaris Sarana dan Prasarana SMK</title>
	<meta name="description" content="Bootstrap Metro Dashboard">
	<meta name="author" content="Dennis Ji">
	<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<!-- end: Meta -->
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	
	<!-- start: CSS -->
	<link id="bootstrap-style" href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->
	

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->
		
	<!-- start: Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico">
	<script type="text/javascript" src="js/Chart.js"></script>
	<!-- end: Favicon -->
	
		
		
		
</head>

<body>
		<!-- start: Header -->
	<div class="navbar">
		<div class="blue">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href=""><span>Inventaris</span></a>
					
				<!-- start: Header Menu -->
				<div class="nav-no-collapse header-nav">
					<ul class="nav pull-right">
						
						
						<!-- start: User Dropdown -->
						<li class="dropdown">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="halflings-icon white user"></i> <?php echo $_SESSION['nama_petugas'];?>
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li><a href="profil.php"><i class="halflings-icon user"></i> Profil</a></li>
								<li><a href="logout.php"><i class="halflings-icon off"></i> Logout</a></li>
							</ul>
						</li>
						<!-- end: User Dropdown -->
					</ul>
				</div>
				<!-- end: Header Menu -->
				
			</div>
		</div>
	</div>
	<!-- start: Header -->
	
		<div class="container-fluid-full">
		<div class="row-fluid">
				
			<!-- start: Main Menu -->
			<div id="sidebar-left" class="span2">
				<div class="nav-collapse sidebar-nav">
					<ul class="nav nav-tabs nav-stacked main-menu">
						<li><a href="dashboard.php"><i class="icon-home"></i><span class="hidden-tablet"> Dashboard</span></a></li>	
						<li><a href="inventaris.php"><i class="icon-folder-open"></i><span class="hidden-tablet"> Inventaris</span></a></li>	
						<li>
							<a class="dropmenu" href="#"><i class="icon-info-sign"></i><span class="hidden-tablet"> Proses</span></a>
							<ul>
								<li><a class="submenu" href="peminjaman.php"><i class="icon-tasks"></i><span class="hidden-tablet"> Peminjaman</span></a></li>
								<li><a class="submenu" href="pengembalian.php"><i class="icon-tasks"></i><span class="hidden-tablet"> Pengembalian</span></a></li>
							</ul>
						</li>
						<li>
							<a class="dropmenu" href="#"><i class="icon-book"></i><span class="hidden-tablet"> Laporan</span></a>
							<ul>
								<li><a class="submenu" href="laporan_pdf.php"></i><span class="hidden-tablet"> Laporan PDF</span></a></li>
								<li><a class="submenu" href="laporan_excel.php"></i><span class="hidden-tablet"> Laporan Excel</span></a></li>
							</ul>
						</li>
						<li>
							<a class="dropmenu" href="#"><i class="icon-folder-open"></i><span class="hidden-tablet"> Arsip</span></a>
							<ul>
								<li><a class="submenu" href="data_pegawai.php"><i class="icon-group"></i><span class="hidden-tablet"> Data Pegawai</span></a></li>
								<li><a class="submenu" href="data_hapus_level.php"><i class="icon-list-alt"></i><span class="hidden-tablet"> Data Level</span></a></li>
								<li><a class="submenu" href="data_hapus_ruang.php"><i class="icon-briefcase"></i><span class="hidden-tablet"> Data Ruang</span></a></li>
								<li><a class="submenu" href="data_hapus_jenis.php"><i class="icon-align-justify"></i><span class="hidden-tablet"> Data Jenis</span></a></li>
								<li><a class="submenu" href="data_petugas.php"><i class="icon-user"></i><span class="hidden-tablet"> Data Petugas</span></a></li>
							</ul>
						</li>
						
						<li><a href="petugas.php"><i class="icon-user"></i><span class="hidden-tablet"> Petugas</span></a></li>
						</ul>
					
				</div>
			</div>
			<!-- end: Main Menu -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<!-- start: Content -->
			<div id="content" class="span10">
			<div class="row-fluid sortable">
				<h2>Laporan Data PDF<h2>
				<br>
				<div class="span3 statbox red" onTablet="span6" onDesktop="span3">
					<div class="number"><i class="icon-folder-open"></i></div>
					<div class="title">Data Inventaris</div>
					<div class="footer"><a href="laporan_inventaris.php"><i class="icon-arrow-right"> Cetak</i></a>
					</div>	
				</div>
				<div class="span3 statbox green" onTablet="span6" onDesktop="span3">
					<div class="number"><i class="icon-reorder"></i></div>
					<div class="title">Data Jenis</div>
					<div class="footer"><a href="laporan_jenis.php"><i class="icon-arrow-right"> Cetak</i></a>
					</div>	
				</div>
				<div class="span3 statbox blue" onTablet="span6" onDesktop="span3">
					<div class="number"><i class="icon-briefcase"></i></div>
					<div class="title">Data Ruang</div>
					<div class="footer"><a href="laporan_ruang.php"><i class="icon-arrow-right"> Cetak</i></a>
					</div>	
				</div>
				<div class="span3 statbox purple" onTablet="span6" onDesktop="span3">
					<div class="number"><i class="icon-group"></i></div>
					<div class="title">Data Pegawai</div>
					<div class="footer"><a href="laporan_pegawai.php"><i class="icon-arrow-right"> Cetak</i></a>
					</div>	
				</div>
				<div class="span3 statbox orange" onTablet="span6" onDesktop="span3">
					<div class="number"><i class="icon-user"></i></div>
					<div class="title">Data Petugas</div>
					<div class="footer"><a href="laporan_petugas.php"><i class="icon-arrow-right"> Cetak</i></a>
					</div>	
				</div>
				<div class="span3 statbox black" onTablet="span6" onDesktop="span3">
					<div class="number"><i class="icon-calendar"></i></div>
					<div class="title">Data Pertanggal</div>
					<div class="footer"><a href="laporan_tanggal.php"><i class="icon-arrow-right"> Lihat</i></a>
					</div>	
				</div>
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
	<div class="common-modal modal fade" id="common-Modal1" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-content">
			<ul class="list-inline item-details">
				<li><a href="http://themifycloud.com">Admin templates</a></li>
				<li><a href="http://themescloud.org">Bootstrap themes</a></li>
			</ul>
		</div>
	</div>
	<div class="clearfix"></div>
	
	<footer>

		<p>
			<span style="text-align:left;float:left">&copy; 2019 Inventaris Sarana dan Prasarana SMK</a></span>
			
		</p>

	</footer>
	
	<!-- start: JavaScript-->

		<script src="js/jquery-1.9.1.min.js"></script>
	<script src="js/jquery-migrate-1.0.0.min.js"></script>
	
		<script src="js/jquery-ui-1.10.0.custom.min.js"></script>
	
		<script src="js/jquery.ui.touch-punch.js"></script>
	
		<script src="js/modernizr.js"></script>
	
		<script src="js/bootstrap.min.js"></script>
	
		<script src="js/jquery.cookie.js"></script>
	
		<script src='js/fullcalendar.min.js'></script>
	
		<script src='js/jquery.dataTables.min.js'></script>

		<script src="js/excanvas.js"></script>
	<script src="js/jquery.flot.js"></script>
	<script src="js/jquery.flot.pie.js"></script>
	<script src="js/jquery.flot.stack.js"></script>
	<script src="js/jquery.flot.resize.min.js"></script>
	
		<script src="js/jquery.chosen.min.js"></script>
	
		<script src="js/jquery.uniform.min.js"></script>
		
		<script src="js/jquery.cleditor.min.js"></script>
	
		<script src="js/jquery.noty.js"></script>
	
		<script src="js/jquery.elfinder.min.js"></script>
	
		<script src="js/jquery.raty.min.js"></script>
	
		<script src="js/jquery.iphone.toggle.js"></script>
	
		<script src="js/jquery.uploadify-3.1.min.js"></script>
	
		<script src="js/jquery.gritter.min.js"></script>
	
		<script src="js/jquery.imagesloaded.js"></script>
	
		<script src="js/jquery.masonry.min.js"></script>
	
		<script src="js/jquery.knob.modified.js"></script>
	
		<script src="js/jquery.sparkline.min.js"></script>
	
		<script src="js/counter.js"></script>
	
		<script src="js/retina.js"></script>

		<script src="js/custom.js"></script>
	<!-- end: JavaScript-->
	
</body>
</html>
