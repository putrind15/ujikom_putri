<!DOCTYPE html>
<?php
include 'cek.php';
?>
<html class="no-js">
    
   <head>
        <title>Inventaris Sarana Dan Prasarana SMK</title>
        <!-- Bootstrap -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="vendors/flot/excanvas.min.js"></script><![endif]-->
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar">Inventaris</span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
					 <a class="brand" href="#">Inventaris</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i> <?php echo $_SESSION['nama_petugas'];?> <i class="caret"></i>

                                </a>
                                <ul class="dropdown-menu">
                                   <li class="divider"></li>
                                    <li>
									<a href="#">Profil</a>
                                        <a href="logout.php"> Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                       
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
		
		<div class="container-fluid">
            <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li class="active">
                            <a href="#"><i class="icon-chevron-right"></i> Beranda</a>
                        </li>
                        <li class="active">
                            <a href="peminjaman.php"><i class="icon-chevron-right"></i> peminjaman</a>
                        </li>
                    </ul>
                </div>
		<div class="span9" id="content">
        <div class="container-fluid">
            <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
						
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Input Peminjaman</div>
                            </div>
							<?php
							include "koneksi.php";
						$select_inventaris=mysql_query("SELECT `id_inventaris`,`nama` FROM `inventaris`");
						$select_pegawai=mysql_query("SELECT `id_pegawai`,`nama_pegawai` FROM `pegawai`");
						?>
						<form action="simpan_peminjaman.php" method="post" role="form" class="form-horizontal">
						  
                            <div class="block-content collapse in">
                                <div class="span12">
                                    <form class="form-horizontal">
                                      <fieldset>
                                       <div class="control-group">
                                          <label class="control-label" for="typeahead">Jumlah </label>
                                          <div class="controls">
                                            <input type="number" class="span6" id="typeahead" data-provide="typeahead" data-items="4" data-source='["Alabama","Alaska","Arizona","Arkansas","California","Colorado","Connecticut","Delaware","Florida","Georgia","Hawaii","Idaho","Illinois","Indiana","Iowa","Kansas","Kentucky","Louisiana","Maine","Maryland","Massachusetts","Michigan","Minnesota","Mississippi","Missouri","Montana","Nebraska","Nevada","New Hampshire","New Jersey","New Mexico","New York","North Dakota","North Carolina","Ohio","Oklahoma","Oregon","Pennsylvania","Rhode Island","South Carolina","South Dakota","Tennessee","Texas","Utah","Vermont","Virginia","Washington","West Virginia","Wisconsin","Wyoming"]'>
                                           </div>
                                        </div>
										
                                        <div class="control-group">
                                          <label class="control-label" for="select01">Nama Barang</label>
                                          <div class="controls">
                                            <select id="select01" name="id_inventaris" class="chzn-select span6">
                                              <option value="">Pilih Barang</option>
											  <?php while($data_inventaris=mysql_fetch_array($select_inventaris)){ ?>
											  <option value="<?php echo $data_inventaris['id_inventaris']; ?>"><?php echo $data_inventaris['nama'];?>
											  </option>
											  <?php } ?>
                                            </select>
                                          </div>
                                        </div>
										
									<div class="control-group">
  								<label class="control-label">Status Peminjaman<span class="required"></span></label>
  								<div class="controls">
  									<select class="span6 m-wrap" name="status_peminjaman">
										<option value="">Pilih Status</option>
  										<option>Pinjam</option>
  										<option>Kembali</option>
  									</select>
  								</div>
  							</div>
										
										<div class="control-group">
                                          <label class="control-label" for="select01">Pegawai</label>
                                          <div class="controls">
                                            <select id="select01" name="id_pegawai" class="chzn-select span6">
                                              <option value="">Pilih Pegawai</option>
											  <?php while($data_pegawai=mysql_fetch_array($select_pegawai)){ ?>
											  <option value="<?php echo $data_pegawai['id_pegawai']; ?>"><?php echo $data_pegawai['nama_pegawai'];?>
											  </option>
											  <?php } ?>
                                            </select>
                                          </div>
                                        </div>
                                        <div class="form-actions">
                                          <button type="submit" class="btn btn-primary">Save changes</button>
                                          <button type="reset" class="btn">Cancel</button>
                                        </div>
                                      </fieldset>
                                    </form>

                                </div>
                            </div>
                        </div>
            </div>
						
            <hr>
            <footer>
                <p>2019 Inventaris Sarana dan Prasarana SMK</p>
            </footer>
        </div>
        </div>
        <!--/.fluid-container-->

        <link href="vendors/datepicker.css" rel="stylesheet" media="screen">
        <link href="vendors/uniform.default.css" rel="stylesheet" media="screen">
        <link href="vendors/chosen.min.css" rel="stylesheet" media="screen">

        <link href="vendors/wysiwyg/bootstrap-wysihtml5.css" rel="stylesheet" media="screen">

        <script src="vendors/jquery-1.9.1.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/jquery.uniform.min.js"></script>
        <script src="vendors/chosen.jquery.min.js"></script>
        
        <script src="vendors/bootstrap-datepicker.js"></script>

        <script src="vendors/wysiwyg/wysihtml5-0.3.0.js"></script>
        <script src="vendors/wysiwyg/bootstrap-wysihtml5.js"></script>

        <script src="vendors/wizard/jquery.bootstrap.wizard.min.js"></script>

	<script type="text/javascript" src="vendors/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="assets/form-validation.js"></script>
        
	<script src="assets/scripts.js"></script>
        <script>

	jQuery(document).ready(function() {   
	   FormValidation.init();
	});
	

        $(function() {
            $(".datepicker").datepicker();
            $(".uniform_on").uniform();
            $(".chzn-select").chosen();
            $('.textarea').wysihtml5();

            $('#rootwizard').bootstrapWizard({onTabShow: function(tab, navigation, index) {
                var $total = navigation.find('li').length;
                var $current = index+1;
                var $percent = ($current/$total) * 100;
                $('#rootwizard').find('.bar').css({width:$percent+'%'});
                // If it's the last tab then hide the last button and show the finish instead
                if($current >= $total) {
                    $('#rootwizard').find('.pager .next').hide();
                    $('#rootwizard').find('.pager .finish').show();
                    $('#rootwizard').find('.pager .finish').removeClass('disabled');
                } else {
                    $('#rootwizard').find('.pager .next').show();
                    $('#rootwizard').find('.pager .finish').hide();
                }
            }});
            $('#rootwizard .finish').click(function() {
                alert('Finished!, Starting over!');
                $('#rootwizard').find("a[href*='tab1']").trigger('click');
            });
        });
        </script>
    </body>

</html>