-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 07, 2019 at 08:14 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ujikom`
--

-- --------------------------------------------------------

--
-- Table structure for table `dashboard`
--

CREATE TABLE `dashboard` (
  `id_dashboard` int(11) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dashboard`
--

INSERT INTO `dashboard` (`id_dashboard`, `keterangan`) VALUES
(1, '<p>Inventaris adalah kegiatan untuk mencatat dan menyusun barang barang / bahan yang ada secara benar menurut ketentuan yang berlaku. Dan sebagai sarana dan prasarana pendidikan adalah pencatatan atau pendaftaran barang-barang milik sekolah ke dalam suatu daftar inventaris barang secara tertib dan teratur.&nbsp;</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `detail_pinjam`
--

CREATE TABLE `detail_pinjam` (
  `id_detail_pinjam` int(15) NOT NULL,
  `id_inventaris` int(15) NOT NULL,
  `jumlah_pinjam` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_pinjam`
--

INSERT INTO `detail_pinjam` (`id_detail_pinjam`, `id_inventaris`, `jumlah_pinjam`) VALUES
(4, 2, 2),
(5, 3, 1),
(6, 2, 1),
(7, 5, -9),
(8, 1, 10),
(9, 2, 1),
(10, 2, 1),
(11, 4, 1),
(12, 5, 1),
(13, 2, 1),
(14, 3, 1),
(15, 3, 1),
(16, 2, 1),
(17, 1, 5),
(18, 1, 5),
(19, 1, 1),
(20, 2, 1),
(21, 1, 1),
(23, 4, 1),
(24, 2, 1),
(25, 2, 1),
(26, 2, 2),
(27, 2, 5),
(28, 2, 12),
(29, 1, 2),
(30, 1, 2),
(31, 2, 2),
(32, 2, 1),
(33, 2, 1),
(34, 1, 1),
(35, 4, 2),
(36, 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `inventaris`
--

CREATE TABLE `inventaris` (
  `id_inventaris` int(15) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `kondisi` varchar(20) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `jumlah` float NOT NULL,
  `id_jenis` int(15) NOT NULL,
  `tanggal_register` datetime NOT NULL,
  `id_ruang` int(15) NOT NULL,
  `kode_inventaris` varchar(20) NOT NULL,
  `id_petugas` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventaris`
--

INSERT INTO `inventaris` (`id_inventaris`, `nama`, `kondisi`, `keterangan`, `jumlah`, `id_jenis`, `tanggal_register`, `id_ruang`, `kode_inventaris`, `id_petugas`) VALUES
(1, 'Kursi', 'baik', 'blablaa', 10, 1, '2019-02-01 00:00:00', 5, 'P908JJ', 4),
(2, 'Laptop', 'baik', 'blabla', 20, 1, '2019-02-20 00:00:00', 3, 'KHF093', 2),
(3, 'Komputer', 'baik', 'farhan', 42, 1, '2019-02-05 00:00:00', 5, 'KHG678', 2),
(4, 'Bangku', 'baik', 'blabla', 28, 6, '2019-02-20 06:27:49', 3, 'KHG674', 1),
(5, 'Papan Tulis', 'baik', 'blabla', 10, 6, '2019-04-04 04:45:33', 6, 'KHG009', 2);

-- --------------------------------------------------------

--
-- Table structure for table `jenis`
--

CREATE TABLE `jenis` (
  `id_jenis` int(15) NOT NULL,
  `nama_jenis` varchar(50) NOT NULL,
  `kode_jenis` varchar(50) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `status_hapus` enum('1','0') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis`
--

INSERT INTO `jenis` (`id_jenis`, `nama_jenis`, `kode_jenis`, `keterangan`, `status_hapus`) VALUES
(1, 'Komputer', 'K0992WF', 'blablaa', '1'),
(2, 'laptop', 'KHF093', 'baik', '1'),
(5, 'elektronik', 'KLM214', 'baik', '1'),
(6, 'Kayu', 'KHY344', 'blabla', '1'),
(7, 'kayu bambu', 'RNG215', 'baik', '0');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `id_level` int(15) NOT NULL,
  `nama_level` varchar(50) NOT NULL,
  `status_hapus` enum('1','0') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id_level`, `nama_level`, `status_hapus`) VALUES
(1, 'Administrator', '1'),
(2, 'Operator', '1'),
(3, 'Peminjam', '1'),
(6, 'Guru', '0');

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id_pegawai` int(15) NOT NULL,
  `nama_pegawai` varchar(50) NOT NULL,
  `nip` int(20) NOT NULL,
  `alamat` text NOT NULL,
  `status` enum('Aktif','Tidak Aktif') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nama_pegawai`, `nip`, `alamat`, `status`) VALUES
(1, 'lastri', 1116672829, 'ciomas permai', 'Aktif'),
(2, 'lita', 300998822, 'jl pagelaran', 'Aktif'),
(3, 'Nurul Putri', 32245888, 'ciomas', 'Aktif'),
(4, 'Sani Lestari', 32245887, 'ciomas', 'Aktif'),
(5, 'Finka Aprillia', 8777722, 'Cibalagung', 'Aktif'),
(6, 'Nur Fitria', 32245891, 'Ciherang', 'Aktif'),
(7, 'Resky Saepudin', 32245889, 'Cibereum', 'Aktif'),
(8, 'nadia putri', 87777225, 'ciomas', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `peminjaman`
--

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(15) NOT NULL,
  `tanggal_pinjam` datetime NOT NULL,
  `tanggal_kembali` datetime NOT NULL,
  `status_peminjaman` varchar(15) NOT NULL DEFAULT 'pinjam',
  `id_pegawai` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `peminjaman`
--

INSERT INTO `peminjaman` (`id_peminjaman`, `tanggal_pinjam`, `tanggal_kembali`, `status_peminjaman`, `id_pegawai`) VALUES
(4, '2019-04-04 04:16:17', '2019-04-04 04:16:17', 'Kembali', 2),
(5, '2019-04-04 04:27:39', '2019-04-04 04:27:39', 'Kembali', 3),
(6, '2019-04-04 04:28:08', '2019-04-04 04:28:08', 'Kembali', 6),
(8, '2019-04-04 09:10:53', '2019-04-04 09:10:53', 'Kembali', 8),
(9, '2019-04-05 04:03:38', '2019-04-05 04:03:38', 'Kembali', 1),
(10, '2019-04-05 04:05:51', '2019-04-05 04:05:51', 'Kembali', 3),
(11, '2019-04-05 04:07:01', '2019-04-05 04:07:01', 'Kembali', 5),
(12, '2019-04-05 04:12:25', '2019-04-05 04:12:25', 'Kembali', 6),
(13, '2019-04-05 04:13:43', '2019-04-05 04:13:43', 'Kembali', 7),
(14, '2019-04-05 04:18:29', '2019-04-05 04:18:29', 'Kembali', 7),
(15, '2019-04-05 04:20:38', '2019-04-05 04:20:38', 'Kembali', 1),
(16, '2019-04-05 04:23:08', '2019-04-05 04:23:08', 'Kembali', 3),
(17, '2019-04-05 04:25:38', '2019-04-05 04:25:38', 'Kembali', 2),
(18, '2019-04-05 04:28:52', '2019-04-05 04:28:52', 'Kembali', 1),
(19, '2019-04-05 04:31:45', '2019-04-05 04:31:45', 'Kembali', 1),
(20, '2019-04-05 04:37:14', '2019-04-05 04:37:14', 'Kembali', 4),
(21, '2019-04-05 05:03:16', '2019-04-05 05:03:16', 'Kembali', 1),
(22, '2019-04-05 05:07:02', '2019-04-05 05:07:02', 'Kembali', 4),
(23, '2019-04-05 05:15:37', '2019-04-05 05:15:37', 'Kembali', 5),
(24, '2019-04-05 05:24:00', '2019-04-05 05:24:00', 'Kembali', 1),
(25, '2019-04-05 05:26:06', '2019-04-05 05:26:06', 'Kembali', 6),
(26, '2019-04-05 05:28:05', '2019-04-05 05:28:05', 'Kembali', 3),
(27, '2019-04-05 05:30:30', '2019-04-05 05:30:30', 'Kembali', 2),
(28, '2019-04-05 05:32:24', '2019-04-05 05:32:24', 'Kembali', 5),
(29, '2019-04-05 05:39:22', '2019-04-05 05:39:22', 'Kembali', 1),
(30, '2019-04-05 05:41:22', '2019-04-05 05:41:22', 'Kembali', 1),
(31, '2019-04-05 05:44:09', '2019-04-05 05:44:09', 'Kembali', 1),
(32, '2019-04-05 05:46:28', '2019-04-05 05:46:28', 'Kembali', 2),
(33, '2019-04-05 05:46:57', '2019-04-05 05:46:57', 'Kembali', 2),
(34, '2019-04-05 05:51:11', '2019-04-05 05:51:11', 'Pinjam', 3),
(35, '2019-04-06 16:33:46', '2019-04-06 16:33:46', 'Pinjam', 5),
(36, '2019-04-07 06:33:04', '2019-04-07 06:33:04', 'Kembali', 4);

-- --------------------------------------------------------

--
-- Table structure for table `petugas`
--

CREATE TABLE `petugas` (
  `id_petugas` int(15) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(20) NOT NULL,
  `nama_petugas` varchar(50) NOT NULL,
  `id_level` int(15) NOT NULL,
  `id_pegawai` int(15) NOT NULL,
  `status_hapus_p` enum('1','0') NOT NULL,
  `email` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `petugas`
--

INSERT INTO `petugas` (`id_petugas`, `username`, `password`, `nama_petugas`, `id_level`, `id_pegawai`, `status_hapus_p`, `email`) VALUES
(1, 'admin', 'admin', 'reni rere', 1, 1, '1', ''),
(2, 'operator', 'operator', 'Riskyawati', 2, 1, '1', ''),
(3, 'peminjam', 'peminjam', 'selvi', 3, 1, '1', ''),
(4, 'putri', 'putrind', 'Putri Nadia', 1, 3, '1', ''),
(5, 'adminn', 'admin2', 'Resky', 1, 4, '1', 'putri14nadia@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `ruang`
--

CREATE TABLE `ruang` (
  `id_ruang` int(15) NOT NULL,
  `nama_ruang` varchar(50) NOT NULL,
  `kode_ruang` varchar(15) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `status_hapus` enum('1','0') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ruang`
--

INSERT INTO `ruang` (`id_ruang`, `nama_ruang`, `kode_ruang`, `keterangan`, `status_hapus`) VALUES
(3, 'lab animasi', '17931', 'blabla', '1'),
(4, 'jjj', '777', 'baik', '1'),
(5, 'lab rpl', 'PJK232', 'baik', '1'),
(6, 'lab bahasa', 'PQJ544', 'baik', '0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dashboard`
--
ALTER TABLE `dashboard`
  ADD PRIMARY KEY (`id_dashboard`);

--
-- Indexes for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  ADD PRIMARY KEY (`id_detail_pinjam`),
  ADD KEY `id_inventaris` (`id_inventaris`);

--
-- Indexes for table `inventaris`
--
ALTER TABLE `inventaris`
  ADD PRIMARY KEY (`id_inventaris`),
  ADD KEY `id_jenis` (`id_jenis`),
  ADD KEY `id_ruang` (`id_ruang`),
  ADD KEY `id_petugas` (`id_petugas`);

--
-- Indexes for table `jenis`
--
ALTER TABLE `jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD PRIMARY KEY (`id_peminjaman`),
  ADD KEY `id_pegawai` (`id_pegawai`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`id_petugas`),
  ADD KEY `id_level` (`id_level`),
  ADD KEY `id_pegawai` (`id_pegawai`);

--
-- Indexes for table `ruang`
--
ALTER TABLE `ruang`
  ADD PRIMARY KEY (`id_ruang`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dashboard`
--
ALTER TABLE `dashboard`
  MODIFY `id_dashboard` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  MODIFY `id_detail_pinjam` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `inventaris`
--
ALTER TABLE `inventaris`
  MODIFY `id_inventaris` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `jenis`
--
ALTER TABLE `jenis`
  MODIFY `id_jenis` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id_level` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id_pegawai` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `peminjaman`
--
ALTER TABLE `peminjaman`
  MODIFY `id_peminjaman` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `petugas`
--
ALTER TABLE `petugas`
  MODIFY `id_petugas` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `ruang`
--
ALTER TABLE `ruang`
  MODIFY `id_ruang` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  ADD CONSTRAINT `detail_pinjam_ibfk_1` FOREIGN KEY (`id_inventaris`) REFERENCES `inventaris` (`id_inventaris`) ON UPDATE CASCADE;

--
-- Constraints for table `inventaris`
--
ALTER TABLE `inventaris`
  ADD CONSTRAINT `inventaris_ibfk_1` FOREIGN KEY (`id_jenis`) REFERENCES `jenis` (`id_jenis`) ON UPDATE CASCADE,
  ADD CONSTRAINT `inventaris_ibfk_2` FOREIGN KEY (`id_ruang`) REFERENCES `ruang` (`id_ruang`) ON UPDATE CASCADE,
  ADD CONSTRAINT `inventaris_ibfk_3` FOREIGN KEY (`id_petugas`) REFERENCES `petugas` (`id_petugas`) ON UPDATE CASCADE;

--
-- Constraints for table `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD CONSTRAINT `peminjaman_ibfk_1` FOREIGN KEY (`id_pegawai`) REFERENCES `pegawai` (`id_pegawai`) ON UPDATE CASCADE;

--
-- Constraints for table `petugas`
--
ALTER TABLE `petugas`
  ADD CONSTRAINT `petugas_ibfk_1` FOREIGN KEY (`id_level`) REFERENCES `level` (`id_level`) ON UPDATE CASCADE,
  ADD CONSTRAINT `petugas_ibfk_2` FOREIGN KEY (`id_pegawai`) REFERENCES `pegawai` (`id_pegawai`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
